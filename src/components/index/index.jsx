
import React, { Component } from 'react';
import './index.css';
import Experience from '../experience/experience';
class Index extends Component {

  state = {
    get_country: [],
    country_items: [],
    to: true,
    loading: false,
    country: [{
      "id": "1",
      "value": "Afghanistan",
      "label": "Afghanistan"
    }, {
      "id": "17",
      "value": "Albania",
      "label": "Albania"
    }, {
      "id": "18",
      "value": "Algeria",
      "label": "Algeria"
    }, {
      "id": "20",
      "value": "American Samoa",
      "label": "American Samoa"
    }, {
      "id": "22",
      "value": "Andorra",
      "label": "Andorra"
    }, {
      "id": "10",
      "value": "Angola",
      "label": "Angola"
    }, {
      "id": "11",
      "value": "Anguilla",
      "label": "Anguilla"
    }, {
      "id": "23",
      "value": "Antarctica",
      "label": "Antarctica"
    }, {
      "id": "24",
      "value": "Antigua and Barbuda",
      "label": "Antigua and Barbuda"
    }, {
      "id": "25",
      "value": "Argentina",
      "label": "Argentina"
    }, {
      "id": "26",
      "value": "Armenia",
      "label": "Armenia"
    }, {
      "id": "27",
      "value": "Aruba",
      "label": "Aruba"
    }, {
      "id": "28",
      "value": "Australia",
      "label": "Australia"
    }, {
      "id": "29",
      "value": "Austria",
      "label": "Austria"
    }, {
      "id": "12",
      "value": "Azerbaijan",
      "label": "Azerbaijan"
    }],
    data : {
      experience:
        [
          [{ id: 1, title: 'Lorem Ipsum is simply', desc: 'e industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets cont', date: 'Dec. 2017 - 2019' }],
          [
            { id: 2, title: 'Lorem Ipsum is simply', desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', date: 'Dec. 2017 - 2019' },
            { id: 3, title: 'Lorem Ipsum is simply', desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry', date: 'Dec. 2017 - 2019' },
            { id: 4, title: 'Lorem Ipsum is simply', desc: 'Lorem Ipsum is simply dummy text of the printing and', date: 'Dec. 2017 - 2019' }
          ],
          [{ id: 5, title: 'Lorem Ipsum is simply', desc: 'e industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets cont', date: 'Dec. 2017 - 2019' }]
        ]
    }
  };


  componentDidMount() {
    this.setState({ get_country: this.state.country });
  }
  render() {

    const listItems = this.state.get_country.map((item) =>
      <div key={item.id} onClick={(v) => this.set(item.value)}>{item.label}</div>
    );

    let li;
    let button;
    if (this.state.to) {
      li = <li>
        <span className="to">To</span>
        <div className="select year" id="year" onClick={(e) => this.collapse(e)}>
          <div>Year</div>
          <div className="scroll">
            <div onClick={(e) => this.select(e)}>1990</div>
            <div onClick={(e) => this.select(e)}>1991</div>
            <div onClick={(e) => this.select(e)}>1992</div>
            <div onClick={(e) => this.select(e)}>1993</div>
            <div onClick={(e) => this.select(e)}>1994</div>
          </div>
        </div>
        <div className="select month" onClick={(e) => this.collapse(e)}>
          <div>Month</div>
          <div className="scroll">
            <div onClick={(e) => this.select(e)}>Jan</div>
            <div onClick={(e) => this.select(e)}>Feb</div>
            <div onClick={(e) => this.select(e)}>Mar</div>
            <div onClick={(e) => this.select(e)}>Apr</div>
            <div onClick={(e) => this.select(e)}>May</div>
            <div onClick={(e) => this.select(e)}>Jun</div>
            <div onClick={(e) => this.select(e)}>Jul</div>
            <div onClick={(e) => this.select(e)}>Aug</div>
            <div onClick={(e) => this.select(e)}>Sept</div>
            <div onClick={(e) => this.select(e)}>Oct</div>
            <div onClick={(e) => this.select(e)}>Nov</div>
            <div onClick={(e) => this.select(e)}>Dec</div>
          </div>
        </div>
      </li>
    }
    if (this.state.loading) {
      button = <span className="spinner" ></span>
    }
    else {
      button = <span className="button" onClick={(e) => this.save(e)}>Save</span>
    }
    return (

      <div id="container">
        <div className="page-item">
          <label className="infotext">Info</label>
        </div>
        <div className="page-item">
          <input type="text" defaultValue="" placeholder="Full Name" id="fullname" />
        </div>
        <div className="page-item">
          <div className="select country">
            <input type="text" defaultValue="" placeholder="Country" id="country" autoComplete="off" onChange={(e) => this.filter(e)} onClick={(e) => this.autocomplate(e)} />
            <div className="scroll">
              {listItems}
            </div>
          </div>
        </div>

        <div className="page-item">
          <ul id="date-piker">
            <li>
              <span className="from">From</span>
              <div className="select year" id="year-from" onClick={(e) => this.collapse(e)}>
                <div>Year</div>
                <div className="scroll">
                  <div onClick={(e) => this.select(e)}>1990</div>
                  <div onClick={(e) => this.select(e)}>1991</div>
                  <div onClick={(e) => this.select(e)}>1992</div>
                  <div onClick={(e) => this.select(e)}>1993</div>
                  <div onClick={(e) => this.select(e)}>1994</div>
                </div>
              </div>
              <div className="select month" onClick={(e) => this.collapse(e)}>
                <div>Month</div>
                <div className="scroll">
                  <div onClick={(e) => this.select(e)}>Jan</div>
                  <div onClick={(e) => this.select(e)}>Feb</div>
                  <div onClick={(e) => this.select(e)}>Mar</div>
                  <div onClick={(e) => this.select(e)}>Apr</div>
                  <div onClick={(e) => this.select(e)}>May</div>
                  <div onClick={(e) => this.select(e)}>Jun</div>
                  <div onClick={(e) => this.select(e)}>Jul</div>
                  <div onClick={(e) => this.select(e)}>Aug</div>
                  <div onClick={(e) => this.select(e)}>Sept</div>
                  <div onClick={(e) => this.select(e)}>Oct</div>
                  <div onClick={(e) => this.select(e)}>Nov</div>
                  <div onClick={(e) => this.select(e)}>Dec</div>
                </div>
              </div>
            </li>
            {li}
          </ul>
        </div>
        <div className="page-item">
          <div className="pure-checkbox">
            <input id="checkbox1" name="checkbox" type="checkbox" onChange={(e) => this.currentliwork(e)} />
            <label htmlFor="checkbox1">I Currently Work</label>
          </div>
        </div>
        <div className="page-item">
          {button}
        </div>

        <Experience data={this.state.data}/>
      </div>
    );

  }

  collapse = (event) => {
    var target = event.target || event.srcElement || event.currentTarget;
    if (target.classList.contains('collapsed')) {
      target.classList.remove('collapsed');
    }
    else {
      target.classList.add('collapsed');
    }
  }

  autocomplate = (event) => {
    var target = event.target || event.srcElement || event.currentTarget;
    target = target.parentElement;//parent of "target"
    if (target.classList.contains('collapsed')) {
      target.classList.remove('collapsed');
    }
    else {
      target.classList.add('collapsed');
    }
  }

  filter = (event) => {
    var target = event.target || event.srcElement || event.currentTarget;
    if (target.value.length <= 0) {
      let el = document.getElementById('country').parentElement;
      el.classList.add('collapsed');
    }
    let c = this.state.country.filter(function (el) {
      return el.label.includes(target.value);
    });
    this.setState({ get_country: c });
  }

  set = (value) => {
    (document.getElementById('country')).value = value;
    let el = document.getElementById('country').parentElement;
    el.classList.remove('collapsed');
  }

  select = (event) => {
    var target = event.target || event.srcElement || event.currentTarget;
    var parent = target.parentElement;//parent of "target"
    parent = parent.parentElement;
    (parent.firstChild).innerHTML = target.innerText;
    parent.classList.remove('collapsed');
  }

  currentliwork = (event) => {
    if (event.currentTarget.checked)
      this.setState({ to: false });
    else this.setState({ to: true });
  }

  save = (e) => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false });
    }, 3000)
  }
}
export default Index