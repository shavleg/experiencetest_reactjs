import React, { Component } from 'react';
import './experience.css';
class Experience extends Component {
    constructor(props) {
        super(props);
    }
    state = {
        data: [],
        get_data: []
    }
    componentDidMount() {
        let d = this.props.data;
        setTimeout(() => {
            this.setState({ data: d });
            this.setState({ get_data: this.state.data.experience.slice(0, 2) })
        }, 1000);
    }
  
    render() {
        let showmore = this.state.data.experience && this.state.data.experience.length > 2 ? <li className="show" onClick={(e) => this.showmore(e)}>Show More</li> : null;

        const listItems = this.state.get_data.map((item) => {
            if (item.length == 1) {
                return (
                    item.map((i) => (
                        <li key={i.id} className="alonedot first">
                            <div className="circle"></div>
                            <div className="title">{i.title}</div>
                            <p className="date">{i.date}</p>
                            <p className="description" id={'desc-' + i.id}><span>{i.desc.length <= 200 ? i.desc : i.desc.length > 200 ? i.desc.substring(0, 200) : i.desc}</span>
                                {i.desc.length > 200 ? <span >&nbsp;...&nbsp; <a onClick={(event) => this.show(i.id, event)}>More</a></span> : null}
                            </p>
                        </li>
                    ))
                )
            }
            if (item.length > 1) {
                return (
                    item.map((i, ind) => {
                        if (ind == 0) {
                            return (<li key={i.id} className="dot first">
                                <div className="circle"></div>
                                <div className="title">{i.title}</div>
                                <p className="date">{i.date}</p>
                                <p className="description" id={'desc-' + i.id}><span>{i.desc.length <= 200 ? i.desc : i.desc.length > 200 ? i.desc.substring(0, 200) : i.desc}</span>
                                    {i.desc.length > 200 ? <span >&nbsp;...&nbsp; <a onClick={(event) => this.show(i.id, event)}>More</a></span> : null}
                                </p>
                            </li>)
                        } if (ind + 1 == item.length) {
                            return (<li key={i.id} className="lastdot">
                            <div className="title">{i.title}</div>
                            <p className="date">{i.date}</p>
                            <p className="description" id={'desc-' + i.id}><span>{i.desc.length <= 200 ? i.desc : i.desc.length > 200 ? i.desc.substring(0, 200) : i.desc}</span>
                                {i.desc.length > 200 ? <span >&nbsp;...&nbsp; <a onClick={(event) => this.show(i.id, event)}>More</a></span> : null}
                            </p>
                        </li>)
                        }
                        if (ind + 1 != item.length && ind + 1 > 1) {
                            return(<li key={i.id} className="dot">
                            <div className="title">{i.title}</div>
                            <p className="date">{i.date}</p>
                            <p className="description" id={'desc-' + i.id}><span>{i.desc.length <= 200 ? i.desc : i.desc.length > 200 ? i.desc.substring(0, 200) : i.desc}</span>
                                {i.desc.length > 200 ? <span >&nbsp;...&nbsp; <a onClick={(event) => this.show(i.id, event)}>More</a></span> : null}
                            </p>
                        </li>)
                        }
                    }
                    )
                )
            }
        }
        );

        return (
            <ul className="bullet">
                {listItems}
                {showmore}
            </ul>
        );
    }

    show(id, event) {
        console.log(event)
        var target = event.target || event.srcElement || event.currentTarget;
        if ((document.getElementById('desc-' + id).firstChild).innerHTML.length - 11 > 200) {
            for (var k in this.state.data.experience) {
                let d = this.state.data.experience[k].find(item => item.id === id);
                if (d) {
                    (document.getElementById('desc-' + id).firstChild).innerHTML = d.desc.substring(0, 200);
                    target.innerHTML = 'More';
                }
            }

        }
        else if ((document.getElementById('desc-' + id).firstChild).innerHTML.length - 11 <= 200) {
            for (var k in this.state.data.experience) {
                let d = this.state.data.experience[k].find(item => item.id === id);
                if (d) {
                    (document.getElementById('desc-' + id).firstChild).innerHTML = d.desc;
                    target.innerHTML = 'Less';
                }
            }

        }
    }

    showmore(event) {
        var target = event.target || event.srcElement || event.currentTarget;
        if (this.state.get_data.length <= 2) {
            this.setState({ get_data: this.state.data.experience });
            target.innerHTML = "Show Less";
        }
        else {
            this.setState({ get_data: this.state.data.experience.slice(0, 2) });
            target.innerHTML = "Show More";
        }
    }
}
export default Experience;